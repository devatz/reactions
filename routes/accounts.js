const https = require('https');
const express = require('express');
const router = express.Router();
const config = require('../config/database');
const Accounts = require('../models/accounts');
const request = require('request');

router.post('/add', (req, res, next) => {
  if(!req.body.token){
    return res.json({success: false, msg:'Bạn chưa nhập Access token'});
  }
  if(!req.body.reaction){
    return res.json({success: false, msg:'Bạn chưa chọn reactions'});
  }

  const token = req.body.token;
  const reaction = req.body.reaction;

  request('https://graph.facebook.com/me?access_token='+token, function (error, response, body) {
    body = JSON.parse(body);
    if(body.error){
      return res.json({success: false, msg: 'Access token die hoặc không có quyền'});
    }
    else{
      const idfb = body.id;
      const name = body.name;
      Accounts.getAccountByToken(token, (err, account) =>{
        if(account)
          return res.json({success: false, msg: 'Access token này đã có trong hệ thống'});
        else{
          var newAccount = new Accounts({
            idfb: idfb,
            name: name,
            reaction: reaction,
            token: token
          });
          Accounts.addAccount(newAccount, (err, user) => {
            if(err){
              res.json({success: false, msg:'Đã xảy ra lỗi, không thể thêm'});
            } else {
              res.json({success: true, msg:'Thêm thành công tài khoản: ' + name});
            }
          });
        }
      });
    }
  });  
});

router.post('/delete', (req, res, next) => {
  if(!req.body.id){
    return res.json({success: false, msg:'Phải nhập tài khoản mới xóa được'});
  }
  const id = req.body.id;
  Accounts.getAccountById(id, (err, account) =>{
    if(!account)
      return res.json({success: false, msg: 'Không tồn tại tài khoản này'});
    else{
      Accounts.deleteAccount(id, (err, user) => {
        if(err){
          res.json({success: false, msg:'Đã xảy ra lỗi, không thể xóa'});
        } else {
          res.json({success: true, msg:'Đã xóa thành công tài khoản: ' + user.name});
        }
      });
    }
  })
});

module.exports = router;
