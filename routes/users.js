const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');

// Register
router.get('/', (req, res) => {
  res.render('users/register');
});
router.post('/register', (req, res, next) => {
  console.log(req.body);
  if(!req.body.name){
    return res.json({success: false, msg:'Bạn chưa nhập họ tên'});
  }
  if(!req.body.email){
    return res.json({success: false, msg:'Bạn chưa nhập Email'});
  }
  if(!req.body.username){
    return res.json({success: false, msg:'Bạn chưa nhập tài khoản'});
  }
  if(!req.body.password){
    return res.json({success: false, msg:'Bạn chưa nhập mật khẩu'});
  }
  const name = req.body.name;
  const email = req.body.email.toLowerCase();
  const username = req.body.username.toLowerCase();
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if(user){
      return res.json({success: false, msg: 'Tài khoản này đã được sử dụng'});
    }else{
      User.getUserByEmail(email, (err, user) => {
        if(user){
          return res.json({success: false, msg: 'Email này đã được sử dụng'});
        }else{
          var newUser = new User({
            name: name,
            email: email,
            username: username,
            password: password
          });
          User.addUser(newUser, (err, user) => {
            if(err){
              res.json({success: false, msg:'Không thể đăng ký'});
            } else {
              res.json({success: true, msg:'Đăng ký thành công'});
            }
          });
        }
      });
    }
  });
  
});

// Authenticate
router.post('/login', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found'});
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch){
        const token = jwt.sign(user, config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT '+token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email
          }
        });
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    });
  });
});

// Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  res.json({user: req.user});
});

module.exports = router;
