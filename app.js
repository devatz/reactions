const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const app = express();
const users = require('./routes/users');
const accounts = require('./routes/accounts');
const port = process.env.PORT || 801;

mongoose.Promise = global.Promise;
mongoose.connect(config.database);
mongoose.connection.on('connected', () => {
  console.log('Đã kết nối tới:  '+config.database);
});
mongoose.connection.on('error', (err) => {
  console.log('Lỗi database: '+err);
});

app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);
app.use('/users', users);
app.use('/accounts', accounts);
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
    res.render('index');
});

// Start Server
app.listen(port, () => {
  console.log('Server da chay '+port);
});
